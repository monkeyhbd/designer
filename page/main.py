from graphic import page as GUIPage
from widget import paper as WidgetPaper
from widget import colorBar as WidgetColorBar
from widget import mainMenu as WidgetMainMenu
from data.config import X, Y, W


class Page(GUIPage.Page):
    def __init__(self, master):
        GUIPage.Page.__init__(self, master=master)

    def build(self):
        WidgetMainMenu.main_menu(self)
        WidgetColorBar.color_bar(self, 0, 2 * W, 2 * W, Y * W)
        WidgetPaper.paper(self, 2.2 * W, 2 * W, X, Y)


page_instance: Page


def init(master):
    global page_instance
    page_instance = Page(master=master)


def display():
    global page_instance
    page_instance.display()
