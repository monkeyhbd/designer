import tkinter
from swap import data as SwapData
from data.config import W


def color_bar(master, x, y, width, height):
    board = tkinter.Canvas(master, bg='white')
    board.place(x=x, y=y, width=width, height=height)
    known_color = ['black', 'red', 'orange', 'green', 'blue', 'purple',
                   'Pink', 'Crimson', 'PaleVioletRed', 'HotPink', 'DeepPink', 'Violet',
                   'Fuchsia', 'BlueViolet', 'MediumPurple', 'SkyBlue', 'DeepSkyBlue', 'Cyan',
                   'MediumSpringGreen', 'SpringGreen', 'LimeGreen', 'Lime', 'ForestGreen', 'DarkGreen',
                   'Chartreuse', 'GreenYellow', 'OliveDrab', 'Yellow', 'Gold', 'Goldenrod',
                   'Tan', 'DarkOrange', 'Peru', 'Coral', 'OrangeRed', 'Tomato',
                   'Brown', 'SaddleBrown', 'Sienna', 'Chocolate']

    def change_color_function(new_color):
        def md():
            SwapData.current_color = new_color
        return md

    current_x, current_y = 0, 0
    for color in known_color:
        color_button = tkinter.Button(board, bg=color, command=change_color_function(color))
        color_button.place(x=current_x, y=current_y, width=W, height=W)
        current_x += W
        if current_x + W > width:  # New line.
            current_x = 0
            current_y += W
