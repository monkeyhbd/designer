import tkinter
from swap import data as SwapData
from data import config as DataConfig


W = DataConfig.W


class Pixel(tkinter.Button):
    def __init__(self, master, x, y):
        tkinter.Button.__init__(self, master, bg='white', command=self.click)
        self.x = x
        self.y = y
        self.condition = 0  # 0 - Unselected, 1 - Selected

    def click(self):
        if self.condition == 0:  # Unselected
            self['bg'] = SwapData.current_color
            SwapData.data.append([self.x, self.y, SwapData.current_color])
            self.condition = 1
        else:  # Selected
            bg_backup = self['bg']
            self['bg'] = 'white'
            SwapData.data.remove([self.x, self.y, bg_backup])
            self.condition = 0


def paper(master, x, y, w, h):
    SwapData.pixel_board.clear()
    board = tkinter.Canvas(master)
    board.place(x=x, y=y, width=w * W, height=h * W)
    for yy in range(h):
        this_line = []
        for xx in range(w):
            p = Pixel(board, xx, yy)
            p.place(x=xx * W, y=yy * W, width=W, height=W)
            this_line.append(p)
            if xx == 0:
                p['text'] = yy
            if yy == 0:
                p['text'] = xx
        SwapData.pixel_board.append(this_line)
