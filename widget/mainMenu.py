import tkinter
from swap import data as SwapData
from graphic import basic as GUIBasic
from tool import view as ToolView
from data.config import X, Y, W


def main_menu(master):
    master.update()
    bar = tkinter.Canvas(master, bg='white')
    bar.place(x=0, y=0.1 * W, width=master.winfo_width(), height=1.8 * W)

    current_x = 0

    clear_button = tkinter.Button(bar, text='Clear', command=GUIBasic.clear_paper)
    clear_button.place(x=0.2 * W, y=0.2 * W, width=2 * W, height=1.4 * W)
    current_x += 0.2 * W + 2 * W

    print_button = tkinter.Button(bar, text='Print', command=lambda: print(SwapData.data))
    print_button.place(x=current_x + 0.2 * W, y=0.2 * W, width=2 * W, height=1.4 * W)
    current_x += 0.2 * W + 2 * W

    print_without_color_button = tkinter.Button(bar, text='Print without color',
                                                command=lambda: print([x[: 2] for x in SwapData.data]))
    print_without_color_button.place(x=current_x + 0.2 * W, y=0.2 * W, width=6 * W, height=1.4 * W)
    current_x += 0.2 * W + 6 * W

    def view_command():
        print(SwapData.data)
        view_window = ToolView.Application('Editing', SwapData.data, W)
        view_window.display()
        view_window.mainloop()
    view_button = tkinter.Button(bar, text='View', command=view_command)
    view_button.place(x=current_x + 0.2 * W, y=0.2 * W, width=2 * W, height=1.4 * W)
    current_x += 0.2 * W + 2 * W

    def view_turbo_command():
        print(GUIBasic.optimize_data(SwapData.data))
        view_window = ToolView.Application('Editing', GUIBasic.optimize_data(SwapData.data), W)
        view_window.display_turbo()
        view_window.mainloop()
    view_turbo_button = tkinter.Button(bar, text='View Turbo', command=view_turbo_command)
    view_turbo_button.place(x=current_x + 0.2 * W, y=0.2 * W, width=4 * W, height=1.4 * W)
    current_x += 0.2 * W + 4 * W
