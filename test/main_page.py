import tkinter
import page.main as PageMain
from data.config import X, Y, W


if __name__ == '__main__':
    lmy = tkinter.Tk()
    lmy.title('Mokey Designer')
    lmy.geometry('{}x{}'.format(int((X + 2.2) * W), int((Y + 2) * W)))
    PageMain.init(lmy)
    PageMain.display()
    lmy.mainloop()
