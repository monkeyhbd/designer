# **Mokey Designer**



## 简介

Mokey Designer 是一个基于 Python（Tkinter）的软件，用于设计像素风格的图案。

![Designer](http://monkeyhbd.gitee.io/mokey-land/Data/MokeyDesigner.png "Mokey Designer")

Mokey Designer 起初是我为了开发 [贪吃蛇](https://gitee.com/monkeyhbd/snake) 而设计的，[贪吃蛇](https://gitee.com/monkeyhbd/snake) 项目中的像素风格图案、字体和 Logo 都是用 Mokey Designer 生成的。

Mokey Designer 在一段时间的开发中已经趋于稳定，同时考虑到我准备开发下一个像素风格游戏《Battle Art》，我把它从 [贪吃蛇](https://gitee.com/monkeyhbd/snake) 中独立出来，作为独立的开源项目。

Mokey Designer 项目地址 gitee.com/monkeyhbd/designer 。

## 已实现的目标

- 1.0.0 版本
- 优化 导出(Export) 功能

## 实现中的目标

- 整体移动功能

## 待实现的目标

- 增加颜色数
- 增加格数
- 高分屏适配

## 开发者

Monkeyhbd | 2017 - 2022

## 我的开源项目

[Mokey Designer](https://gitee.com/monkeyhbd/designer) -> gitee.com/monkeyhbd/designer

[贪吃蛇](https://gitee.com/monkeyhbd/snake) -> gitee.com/monkeyhbd/snake

[Sudoku (高中写的，很老了，已停止更新)](https://gitee.com/monkeyhbd/sudoku) -> gitee.com/monkeyhbd/sudoku