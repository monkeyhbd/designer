import tkinter
import time


class Application(tkinter.Tk):
    def __init__(self, title, data, w):
        tkinter.Tk.__init__(self)
        self.title('Mokey View - ' + title)
        self.geometry('400x400')
        self.data = data
        self.w = w
        self.stage = None

    def display(self):
        t0 = time.time()
        self.stage = tkinter.Canvas(self)
        w = self.w
        max_x, max_y = 0, 0
        for unit in self.data:
            if unit[0] > max_x:
                max_x = unit[0]
            if unit[1] > max_y:
                max_y = unit[1]
            tkinter.Button(self.stage, bg=unit[2]).place(x=unit[0] * w, y=unit[1] * w, width=w, height=w)
        self.stage.place(x=0, y=0, width=(max_x+1) * w, height=(max_y+1) * w)
        self.stage.update()
        self.geometry('{}x{}'.format((max_x+1) * w, (max_y+1) * w))
        te = time.time()
        print("len(data) = {}.".format(len(self.data)), end=' ')
        print("display use {}s.".format(te - t0))

    def display_turbo(self):
        t0 = time.time()
        self.stage = tkinter.Canvas(self)
        w = self.w
        max_x, max_y = 0, 0
        for unit in self.data:
            if unit[0] + unit[2] > max_x:
                max_x = unit[0] + unit[2]
            if unit[1] + unit[3] > max_y:
                max_y = unit[1] + unit[3]
            tkinter.Button(self.stage, bg=unit[4]).place(x=unit[0] * w, y=unit[1] * w,
                                                         width=unit[2] * w, height=unit[3] * w)
        self.stage.place(x=0, y=0, width=max_x * w, height=max_y * w)
        self.stage.update()
        self.geometry('{}x{}'.format(max_x * w, max_y * w))
        te = time.time()
        print("len(data) = {}.".format(len(self.data)), end=' ')
        print("display use {}s.".format(te - t0))
